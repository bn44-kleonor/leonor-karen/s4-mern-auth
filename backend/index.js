//declare dependencies
const express = require("express");
const app = express();
const { ApolloServer } = require("apollo-server-express");
const mongoose = require("mongoose");
const cors = require("cors");

//file imports
const typeDefs = require("./typeDefs");
const resolvers = require("./resolvers");

//db connection
mongoose.connect("mongodb://localhost:27017/merng_assetmgt", {
	useNewUrlParser: true,
	useUnifiedTopology: true,
	useFindAndModify: false,
	useCreateIndex: true
})
mongoose.connection.once("open", () => {
	console.log("Now connected to local MongoDB server");
})

//apollo server
const server = new ApolloServer({
	typeDefs,
	resolvers,
	playground: true,
	introspection: true
})

//use middleware
server.applyMiddleware({
	app,
	path: "/graphql"
})

app.use(cors());

//initialize server
app.listen(8080, ()=>{
	console.log("Malapit na ako grumaduate");
})

//http://localhost:4300/graphql