const jwt = require("jsonwebtoken");
const secret = "merng_assetmgt";

module.exports.createToken = (user) => {
	let data = {
		_id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}

	return jwt.sign(data, secret, { expiresIn: '2h'})
}