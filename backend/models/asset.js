//dependencies
const mongoose = require("mongoose");
const Schema = mongoose.Schema;

//create a schema
const assetSchema = new Schema({
	name: {
		type: String,
		required: true
	},
	description: {
		type: String,
		default: "default description"
	},
	categoryId: {
		type: String,
		required: true
	}
})

//export schema as model
module.exports = mongoose.model("Asset", assetSchema);