//dependencies
const mongoose = require("mongoose");
const Schema = mongoose.Schema;

//create a schema
const categorySchema = new Schema({
	name: {
		type: String,
		required: true
	},
	createdAt: {
		type: String,
		default: Date.now
	},
	updatedAt: {
		type: String,
		default: "Pending"
	}
})

//export schema as model
module.exports = mongoose.model("Category", categorySchema);