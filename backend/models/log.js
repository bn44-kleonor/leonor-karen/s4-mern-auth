//dependencies
const mongoose = require("mongoose");
const Schema = mongoose.Schema;

//create a schema
const logSchema = new Schema({
	action: {
		type: String,
		required: true
	},
	userId: {
		type: String,
		required: true
	},
	createdAt: {
		type: String,
		default: Date.now
	}
});

//export schema as model
module.exports = mongoose.model("Log", logSchema);