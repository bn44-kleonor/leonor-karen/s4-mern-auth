const Asset = require("../models/asset");
const Category = require("../models/category");
const Request = require("../models/request");
const Log = require("../models/log");
const User = require("../models/user");

//dependency
const bcrypt = require("bcrypt");
const auth = require("../jwt-auth");

const resolvers = {
	Category: {
		assets: ({ _id }, args)=>{
			// console.log(categoryId); //undefined
			// console.log(parent)
			return Asset.find({categoryId: _id})
		}
	},
	Asset: {
		category: ({ categoryId}, args) =>{
			return Category.findById(categoryId)
		},
		requests: ({assetId}, args)=>{
			return Request.find({assetId})
		}
	},
	Log: {
		user: ({ userId}, args) => {
			return User.findById(userId)
		}
	},
	Request: {
		user: ({ userId}, args) => {
			return User.findById(userId)
		},
		asset: ({ assetId}, args)=> {
			return Asset.findById(assetId)
		},
		approver: ({approverId}, args)=>{
			return User.findById(approverId)
		}
	},
	User: {
		logs: ({ userId }, args)=>{
			return Log.find({userId})
		}, 
		requests: ({userId}, args)=>{
			return Request.find({userId})
		},
		approves: ({userId})=>{
			return User.find({userId})
		}
	},
	Query: {
		assets: ()=> {
			return Asset.find({})
		},
		categories: ()=> {
			return Category.find({})
		},
		logs: ()=> {
			return Log.find({})
		},
		requests: ()=> {
			return Request.find({})
		},
		users: () =>{
			return User.find({})
		},
		asset: (parent, {id}) =>{
			return Asset.findById(id)
		},
		category: (parent, {id}) =>{
			return Category.findById(id)
		},
		log: (parent, {id}) =>{
			return Log.findById(id)
		},
		request: (parent, {id}) =>{
			return Request.findById(id)
		},
		user: (parent, {id}) =>{
			return User.findById(id)
		}
	},
	Mutation:{
		storeAsset: (parent, {name, description, categoryId}) => {

			//validation

			let asset = new Asset({
				name, description, categoryId
			})
			return asset.save()
		},
		storeRequest: (parent, { userId, assetId, dateRequested}) => {

			//validation

			let request = new Request({
				userId, assetId, dateRequested
			})

			return request.save().then((user, err) =>{
				return err ?  false : true;
			})
		},
		//hashing of password
		registerUser: (parent, {name, email, password}) => {

			//hash
			let user = new User({
				name, 
				email, 
				password : bcrypt.hashSync(password, 8)
			})
			return user.save()
		},
		//password needs to be unhashed
		//login token needs to be created
		loginUser: (parent, { email, password}) => {

			//validation

			let query = User.findOne({ email });
			return query.then(user => {
				if(user === null) {
					return null
				} 

				//unhash password
				let isPasswordMatched = bcrypt.compareSync(password, user.password);

				//create login token
				if(isPasswordMatched){
					user.token = auth.createToken(user.toObject());
					return user;
				} else {
					return null;
				}

			})
		},
		updateRequestStatus: (parent, { id, isApproved, dateApproved, approverId}) => {

			//validations
			return Request.findByIdAndUpdate(id, {isApproved, dateApproved, approverId}, { new: true })
		},
		updateDateReturned: (parent, {id, dateReturned}) => {

			//validations
			return Request.findByIdAndUpdate(id, { dateReturned}, {new: true})
		}
	}

}

module.exports = resolvers;