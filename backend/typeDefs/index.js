const { gql } = require("apollo-server-express");

const typeDefs = gql`
	type Category {
		id: ID
		name: String
		createdAt: String
		updatedAt: String
		assets: [Asset]
	}

	type Asset {
		id: ID
		name: String
		description: String
		categoryId: String
		category: Category
		requests: [Request]
	}

	type Log {
		id: ID
		action: String
		userId: String
		createdAt: String
		user: User
	}

	type Request {
		id: ID
		userId: String
		assetId: String
		dateRequested: String
		dateApprovedOrDenied: String
		isApproved: Boolean
		approverId: String
		dateReturned: String
		user: User
		asset: Asset
		approver: User
	}

	type User {
		id: ID
		name: String
		email: String
		password: String
		isAdmin: Boolean
		createdAt: String
		updatedAt: String
		token: String
		logs: [Log]
		requests: [Request]
		approves: [Request]
	}

	type Query {
		assets: [Asset!]
		categories: [Category!]
		logs: [Log!]
		requests: [Request!]
		users: [User!]
		asset(id: ID!): Asset
		category(id: ID!): Category
		log(id: ID!): Log
		request(id: ID!): Request
		user(id: ID!): User
	}

	type Mutation {
		storeAsset(
			name: String!
			description: String!
			categoryId: String!
		): Asset

		storeRequest(
			userId: String!
			assetId: String!
			dateRequested: String
		): Request

		registerUser(
			name: String
			email: String
			password: String
		): Boolean

		loginUser(
			email: String
			password: String
		): User

		updateRequestStatus(
			id: ID!
			isApproved: Boolean!
			dateApprovedOrDenied: String!
			approverId: String!
		): Request

		updateDateReturned(
			id: ID!
			dateReturned: String!
		): Request
	}
`;

module.exports = typeDefs;