import React from 'react';

const AssetRequest = props => {
	// console.log(props)
	let asset = props.asset;
	return (
			<form>
                <div className='field'>
                  <label className='label has-text-weight-normal'>
                    Asset Name
                  </label>
                  <div className='control'>
                    <input
                      className='input'
                      type='text'
                      value={asset.name}
                      disabled
                    />
                  </div>
                </div>
                <div className='field'>
                  <label className='label has-text-weight-normal'>
                    Asset Description
                  </label>
                  <div className='control'>
                    <input
                      className='input'
                      type='text'
                      value={asset.description}
                      disabled
                    />
                  </div>
                </div>
                <div className='field'>
                  <label className='label has-text-weight-normal'>
                    Category
                  </label>
                  <div className='control'>
                    <input
                      className='input'
                      type='text'
                      value={asset.category.name}
                      disabled
                    />
                  </div>
                </div>
                <br />
                <div className='field'>
                  <div className='control'>
                    <button type='submit' className='button is-link is-primary'>
                      Request
                    </button>
                    &nbsp;
                  </div>
                </div>
              </form>
	)
}

export default AssetRequest;