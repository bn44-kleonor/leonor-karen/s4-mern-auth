import React, { useState } from 'react';
import { graphql } from 'react-apollo';
import compose from 'lodash.flowright';
import Swal from 'sweetalert2';

import { loginMutation } from '../../graphql/mutations';

const LoginForm = props => {

	const [ email, setEmail ] = useState("");
	const [ password, setPassword ] = useState("");

	//console.log(props);
	const login = e => {
		e.preventDefault();

		props.loginMutation({
			variables: {
				email, password
			}
		})
		.then(response => {
			// console.log(response);
			let user = response.data.loginUser;

			if(user != null){
				localStorage.setItem("name", user.name);
				localStorage.setItem("isAdmin", user.isAdmin);
				localStorage.setItem("token", user.token);
				console.log("login successful");

				//swal
				const Toast = Swal.mixin({
				  toast: true,
				  position: 'top-end',
				  showConfirmButton: false,
				  timer: 3000,
				  timerProgressBar: true,
				  onOpen: (toast) => {
				    toast.addEventListener('mouseenter', Swal.stopTimer)
				    toast.addEventListener('mouseleave', Swal.resumeTimer)
				  }
				})

				Toast.fire({
				  icon: 'success',
				  title: 'Signed in successfully'
				}).then(()=>{
					return window.location = "/dashboard";
				})


				//redirect but with refresh

			} else {
				Swal.fire({
					title: "Login Failed",
					text: "Enter valid credentials",
					icon: "error"
				})
			}
		})
	}

	return (
		<form onSubmit={e => login(e)}>
	      <div className='field'>
	        <label className='label has-text-weight-normal'>Email</label>
	        <div className='control'>
	          <input 
	          	className='input' 
	          	type='email' 
	          	onChange = {e => setEmail(e.target.value)}
	          />
	        </div>
	      </div>
	      <div className='field'>
	        <label className='label has-text-weight-normal'>Password</label>
	        <div className='control'>
	          <input 
	          	className='input' 
	          	type='password' 
	          	onChange = {e => setPassword(e.target.value)}
	          />
	        </div>
	      </div>
	      <br />
	      <div className='field'>
	        <div className='control'>
	          <button type='submit' className='button is-link is-primary'>
	            Login
	          </button>
	          &nbsp;
	        </div>
	      </div>
	    </form>
    )
}

export default compose(
	graphql(loginMutation, {name: "loginMutation"})
	)(LoginForm);