import React from 'react';
import Swal from 'sweetalert2';
import 'react-bulma-components/dist/react-bulma-components.min.css';
import { Card } from 'react-bulma-components';
import AssetRow from '../rows/AssetRow';

const AssetList = props => {
  // console.log(props);

  let row = "";
  if(typeof props.assets === "undefined" || props.assets.length === 0) {
    row = (
      <tr>
        <td colSpan="4">
          <em>No assets found.</em>
        </td>
      </tr>
      )
  } else {
    row = props.assets.map(asset => {
      return <AssetRow asset={asset} key={asset.id} />
    })
  }

	return (
		<Card>
            <Card.Header>
              <Card.Header.Title>Asset List</Card.Header.Title>
            </Card.Header>
            <Card.Content>
              <table className='table is-fullwidth is-bordered is-striped is-hoverable'>
                <thead>
                  <tr>
                    <th>Name</th>
                    <th>Descripton</th>
                    <th>Category</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  { row }
                </tbody>
              </table>
            </Card.Content>
          </Card>
	)
}

export default AssetList;