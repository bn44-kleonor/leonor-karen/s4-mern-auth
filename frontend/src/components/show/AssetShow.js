import React from 'react';
import 'react-bulma-components/dist/react-bulma-components.min.css';
import { Section, Heading, Columns, Card } from 'react-bulma-components';
import Swal from 'sweetalert2';
import { graphql } from 'react-apollo';
import compose from 'lodash.flowright';

import { getAssetQuery } from '../../graphql/queries';
import AssetRequest from '../forms/AssetRequest';



const AssetShow = props => {
	// console.log(props.getAssetQuery);

	const asset = props.getAssetQuery.asset;
	let form = <em>Loading form...</em>;

	if(typeof asset !== "undefined") {
		form = <AssetRequest asset={asset}/>
	}

  return (
  	<Section size='medium'>
      <Heading>Asset</Heading>
      <Columns>
        <Columns.Column className='is-6'>
          <Card>
            <Card.Header>
              <Card.Header.Title>Details</Card.Header.Title>
            </Card.Header>
            <Card.Content>
              { form }
            </Card.Content>
          </Card>
        </Columns.Column>
      </Columns>
    </Section>
  	);
};
 
export default compose(
	graphql(getAssetQuery, {
		options: props => {
			return {
				variables: {
					id: props.match.params.id
				}
			}
		},
		name: "getAssetQuery"
	})
)(AssetShow)