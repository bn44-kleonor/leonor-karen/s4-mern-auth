import { gql } from 'apollo-boost';

const registerUserMutation = gql`
	mutation(
		$name: String!
		$email: String!
		$password: String!
	){
		registerUser(
			name: $name
			email: $email
			password: $password
		)
	}
`;

const loginMutation = gql `
	mutation(
		$email: String!
		$password: String!
	) {
		loginUser(
			email: $email
			password: $password
		) {
			name
			isAdmin
			token
		}
	}
`;


export { registerUserMutation };
