import React, { Fragment } from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import ApolloClient from 'apollo-boost';
import { ApolloProvider } from 'react-apollo';

//pages
import AppNavbar from './partials/AppNavbar';
import AssetPage from './pages/AssetPage';
import RequestPage from './pages/RequestPage';
import NotFoundPage from './pages/NotFoundPage';
import AssetShow from './components/show/AssetShow';
import RegisterPage from './pages/RegisterPage';
import LoginPage from './pages/LoginPage';
import DashboardPage from './pages/DashboardPage';

// apollo client
const client = new ApolloClient({ uri: 'http://localhost:8080/graphql'});

const root = document.querySelector("#root");
const pageComponent = (
	<ApolloProvider  client={client}>
		<BrowserRouter>
			<AppNavbar/>
			<Switch>
				<Route exact path="/" />
				<Route component={AssetPage} exact path="/assets"/>
				<Route component={AssetShow} exact path='/asset/:id' />
				<Route component={RequestPage} exact path="/requests"/>
				<Route component={RegisterPage} exact path="/register" />
				<Route component={LoginPage} path="/login" />
				<Route component={DashboardPage} path="/dashboard" />
				<Route component={NotFoundPage} />
			</Switch>
		</BrowserRouter>
	</ApolloProvider>
);

ReactDOM.render(pageComponent, root);
