import React from 'react'

import 'react-bulma-components/dist/react-bulma-components.min.css'
import { 
    Heading,
    Section
} from 'react-bulma-components'
import '../index.css'

const DashboardPage= () => {
	return (
		<Section className="sectionStyle">
			<Heading>Dashboard</Heading>
		</Section>
	)
}

export default DashboardPage;
