import React from 'react';
import 'react-bulma-components/dist/react-bulma-components.min.css';
import { Section, Heading, Columns } from 'react-bulma-components';
 
import RegisterForm from '../components/forms/RegisterForm';

const RegisterPage = props => {
  return (
    <Section size='medium' className='sectionStyle'>
      <Heading>Register</Heading>
      <Columns>
        <Columns.Column className='is-4'>
          <RegisterForm/>
        </Columns.Column>
      </Columns>
    </Section>
  );
};
 
export default RegisterPage;
