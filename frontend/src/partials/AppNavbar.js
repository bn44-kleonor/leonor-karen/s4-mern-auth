import React from 'react';
import { Link } from 'react-router-dom';
import 'react-bulma-components/dist/react-bulma-components.min.css';
import { Navbar } from 'react-bulma-components';

const AppNavbar = props => {
	return (
		<Navbar className='is-black'>
	      <Navbar.Brand>
	        <Link className="navbar-item" to="/">
	        	<strong>MERN AM</strong>
	        </Link>
	        <Navbar.Burger />
	      </Navbar.Brand>
	      <Navbar.Menu>
	        <Navbar.Container position='end'>
	          <Navbar.Item>Users</Navbar.Item>
	          <Navbar.Item>Categories</Navbar.Item>
	          <Link className="navbar-item" to="/assets">Assets</Link>
	          <Link className="navbar-item" to="/requests">Requests</Link>
	          <Link className="navbar-item" to="/register">Register</Link>
	          <Link className="navbar-item" to="/login">Login</Link>
	        </Navbar.Container>
	      </Navbar.Menu>
	    </Navbar>
	)
}

export default AppNavbar;